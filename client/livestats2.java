/*
http://ncic.netmag.cz/apps/nase/livewebstats.html (in Czech)
*/

import java.util.*;
import java.net.*;
import java.io.*;
import java.awt.*;

/* Live web-server statistics */
/* BAR uprava                 */

public class livestats2 extends java.applet.Applet implements Runnable {

private int minute,minuteavg;
private int hour,houravg;
private int day,dayavg;
private int week,weekavg;
private int month,monthavg;
private int year,total;

private String server;
private int refresh;
private int port;

private Thread runner;
private Image image;
private Graphics draw;
private Font bigfont;
private Font smallfont;
private int w,h;

private String TITLE;
private              Color BACKGROUND=Color.white;
private              Color TITLECOLOR=Color.lightGray;
private              Color TEXTCOLOR=Color.black;
private              Color STATSCOLOR=Color.red;
public final static String APPLETINFO="Live Webstats (BAR) - 1.12 (C) Radim Kolar (hsn@cybermail.net) 1998.";


public void init()
{
 minuteavg=minute=hour=houravg=week=weekavg=month=monthavg=year=total=0;
 String s = getParameter("port");
 if(s==null) s="81";
 port=Integer.parseInt(s);
 s = getParameter("refresh");
 if(s==null) s="60";
 refresh=Integer.parseInt(s);
 if(refresh<20) refresh=20;
 s = getParameter("server");
 if(s==null) server="NETMAG";
     else    server=s;
 s = getParameter("title");
 if(s==null) TITLE="Live webstats"; else TITLE=s;
 s = getParameter("background");
 if (s==null) BACKGROUND=Color.white;
            else  BACKGROUND=parseColorString(s);
 s = getParameter("titlecolor");
 if (s==null) TITLECOLOR=Color.lightGray;
            else  TITLECOLOR=parseColorString(s);
 s = getParameter("textcolor");
 if (s==null) TEXTCOLOR=Color.black;
            else TEXTCOLOR=parseColorString(s);
 s = getParameter("statscolor");
 if (s==null) STATSCOLOR=Color.red;
            else  STATSCOLOR=parseColorString(s);
 
 loaddata();
 ginit();
 drawit();
 server+="/check";
 runner=null;
}

	public void start()
	{
		// user visits the page, create a new thread

		if (runner == null)
		{
			runner = new Thread(this);
			runner.start();
		}
	}
	public void stop()
	{
		// user leaves the page, stop the thread

		if (runner != null && runner.isAlive())
			runner.stop();

		runner = null;
	}


public void run()
{
 while(runner!=null)
 {
 try{                                          
      Thread.sleep(refresh*1000L);
    }
 catch (InterruptedException e) { runner=null;return;}
                                             loaddata();
                                             ginit();
                                             // IBM JIT to neprovede
                                             // nextcheck=System.currentTimeMillis()+refresh*1000L;
                                             drawit();
 }
}

private void ginit()
{
 /* inicializace offscreen bufferu */
 image=createImage(size().width, size().height);
 draw=image.getGraphics();
 w=size().width;
 h=size().height;
 Font f=null;
 FontMetrics fm=null;
 int i;
 for(i=50;i>0;i--)
  {
   f=new Font("Courier",Font.BOLD,i);
   fm = getFontMetrics(f);
   if(fm.stringWidth(TITLE)>w) continue;
   if(fm.getHeight()>h) continue;
   break;
  }
 bigfont=f;
 
 for(i=25;i>0;i--)
  {
   f=new Font("Helvetica",Font.PLAIN,i);
   fm = getFontMetrics(f);
   if(fm.getHeight()>h) continue;
   if(fm.stringWidth(drawstring())>w) continue;
   break;
  }
 smallfont=f;
 
 
}

private String drawstring()
{
 return "Day: "+day+" Week: "+week+" Total: "+total;
}

private void drawstring(int x, int y)
{
 FontMetrics fm=getFontMetrics(smallfont);
 draw.setColor(TEXTCOLOR);
 draw.drawString("Day: ",x,y);
 draw.setColor(STATSCOLOR);
 draw.drawString(""+day,x+fm.stringWidth("Day: "),y);
 draw.setColor(TEXTCOLOR);
 draw.drawString(" Week: ",x+fm.stringWidth("Day: "+day),y);
 draw.setColor(STATSCOLOR);
 draw.drawString(""+week,x+fm.stringWidth("Day: "+day+" Week: "),y);
 draw.setColor(TEXTCOLOR);
 draw.drawString(" Total:",x+fm.stringWidth("Day: "+day+" Week: "+week),y); 
 draw.setColor(STATSCOLOR); 
 draw.drawString(""+total,x+fm.stringWidth("Day: "+day+" Week: "+week+" Total: "),y); 
 
}

private void drawit()
{
  /* pozadi */
  draw.setColor(BACKGROUND);   
  draw.fillRect(0, 0, w, h);
  
  /* titulek */
  draw.setColor(TITLECOLOR);
  draw.setFont(bigfont);
  FontMetrics fm=getFontMetrics(bigfont);
  draw.drawString(TITLE,(w-fm.stringWidth(TITLE))/2,fm.getAscent());
  
  /* hodnoty */
  fm=getFontMetrics(smallfont);
  draw.setFont(smallfont);
//  draw.setColor(TEXTCOLOR);
  drawstring((w-fm.stringWidth(drawstring()))/2,h-fm.getDescent());
//  draw.drawString(drawstring(),(w-fm.stringWidth(drawstring()))/2,h-fm.getDescent());
  
  repaint();
}

public void update(Graphics g)
{
 paint(g);
}

public void paint(Graphics g)
{
   g.drawImage(image, 0, 0, this);
}

private void loaddata()
{
 URL me=getCodeBase();
 String line;
 try{
 DataInputStream dis=new DataInputStream(new BufferedInputStream((
      new URL(me.getProtocol(),me.getHost(),port,"/"+server+"/livestats.txt")).openStream()));
 
 int what=0,value;          
 while ( (line = dis.readLine()) != null)
 {
  what++;
  value=Integer.parseInt(line);
  switch(what)
    {
    case 1:minute=value;break;
    case 2:minuteavg=value;break;
    case 3:hour=value;break;
    case 4:houravg=value;break;
    case 5:day=value;break;
    case 6:dayavg=value;break;
    case 7:week=value;break;
    case 8:weekavg=value;break;
    case 9:month=value;break;
    case 10:monthavg=value;break;
    case 11:year=value;break;    
    case 12:total=value;break;
    default:
    }    
 
 }
 dis.close();

    }
 catch (IOException e) {}      
}

public boolean mouseEnter(Event e, int x, int y){

	showStatus(APPLETINFO);

	return(true);

}//end mouseEnter

public String getAppletInfo()
{
   return APPLETINFO;
}    

    public String[][] getParameterInfo()

    {	

		String[][] info = {

            {"title",       "String",      "Main applet title"},

            {"server",	"String", "Display statistics of which server?"},

            {"refresh",	"int (sec)",	"Check for new stats every XX sec."},
            {"background",  "\"RRGGBB\"", "color setup"},
            {"titlecolor",  "\"RRGGBB\"", "color setup"},
            {"textcolor",  "\"RRGGBB\"", "color setup"},
            {"statscolor",  "\"RRGGBB\"", "color setup"},
            


        };

        return info;

    }


    private Color parseColorString(String colorString)
    {


        if(colorString.length()==6){
            int R = Integer.valueOf(colorString.substring(0,2),16).intValue();
            int G = Integer.valueOf(colorString.substring(2,4),16).intValue();
            int B = Integer.valueOf(colorString.substring(4,6),16).intValue();
            return new Color(R,G,B);
        }
        else return Color.lightGray;
    }

  public boolean mouseDown(Event evt, int x, int y)

  {
        try{
            getAppletContext().showDocument(new URL("http://ncic.netmag.cz/apps/nase/livewebstats.html"));
           }
        catch (MalformedURLException e) {};

	return true;

  }
        
}