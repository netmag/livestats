/*
Radim Kolar (hsn@netmag.cz)
http://ncic.netmag.cz/apps/nase/livewebstats.html (in Czech)
*/

import java.util.*;
import java.net.*;
import java.io.*;
import java.awt.*;

/* Live web-server statistics */


public class livestats extends java.applet.Applet implements Runnable {

private int minute,minuteavg;
private int hour,houravg;
private int day,dayavg;
private int week,weekavg;
private int month,monthavg;
private int year,total;

private String server;
private int refresh;
private int port;

// private long nextcheck;
private Thread runner;
private Image image;
private Graphics draw;
private Font bigfont;
private Font smallfont;
private int w,h;
private int startgr, endgr;

private final static int PARTS=7;
private final static int BARBORDER=5;
private final static int AXISBORDER=2;
private String TITLE;
private String COPYRIGHT;
private              Color BACKGROUND=Color.black;
private              Color TITLECOLOR=Color.white;
private              Color AXIS=Color.white;
private              Color LEGEND=Color.white;
private              Color NUMBERS=Color.white;
private              Color BAR=Color.green;
private              Color BARAVG=Color.red;

public final static String APPLETINFO="Live Webstats 1.12 (C) Radim Kolar (hsn@cybermail.net) 1998.";


public void init()
{
 minuteavg=minute=hour=houravg=week=weekavg=month=monthavg=year=total=0;
 String s = getParameter("port");
 if(s==null) s="81";
 port=Integer.parseInt(s);
 s = getParameter("refresh");
 if(s==null) s="60";
 refresh=Integer.parseInt(s);
 if(refresh<20) refresh=20;
 s = getParameter("server");
 if(s==null) server="NETMAG";
     else    server=s;
 s = getParameter("title");
 if(s==null) TITLE="Live webstats"; else TITLE=s;
 s = getParameter("copyright");
 if(s==null) COPYRIGHT="(c) Radim Kolar 1998"; else COPYRIGHT=s;
 s = getParameter("background");
 if (s==null) BACKGROUND=Color.black;
            else  BACKGROUND=parseColorString(s);
 s = getParameter("titlecolor");
 if (s==null) TITLECOLOR=Color.white;
            else  TITLECOLOR=parseColorString(s);
 s = getParameter("axis");
 if (s==null) AXIS=Color.white;
            else  AXIS=parseColorString(s);
 s = getParameter("legend");
 if (s==null) LEGEND=Color.white;
            else  LEGEND=parseColorString(s);
 s = getParameter("numbers");
 if (s==null) NUMBERS=Color.white;
            else  NUMBERS=parseColorString(s);            
 s = getParameter("bar");
 if (s==null) BAR=Color.green;
            else  BAR=parseColorString(s);            
 s = getParameter("baravg");
 if (s==null) BARAVG=Color.red;
            else  BARAVG=parseColorString(s);            
  
 loaddata();
 ginit();
 drawit();
// nextcheck=System.currentTimeMillis()+refresh*1000L;
 server+="/check";
 runner=null;
}

	public void start()
	{
		// user visits the page, create a new thread

		if (runner == null)
		{
			runner = new Thread(this);
			runner.start();
		}
	}
	public void stop()
	{
		// user leaves the page, stop the thread

		if (runner != null && runner.isAlive())
			runner.stop();

		runner = null;
	}


public void run()
{
 while(runner!=null)
 {
 try{                                          
      Thread.sleep(refresh*1000L);
    }
 catch (InterruptedException e) { runner=null;return;}
                                             loaddata();
                                             ginit();
                                             // IBM JIT to neprovede
                                             // nextcheck=System.currentTimeMillis()+refresh*1000L;
                                             drawit();
 }
}

private void ginit()
{
 /* inicializace offscreen bufferu */
 image=createImage(size().width, size().height);
 draw=image.getGraphics();
 w=size().width;
 h=size().height;
 int partsize=h/PARTS;
 Font f=null;
 FontMetrics fm=null;
 int i;
 for(i=50;i>0;i--)
  {
   f=new Font("Courier",Font.BOLD,i);
   fm = getFontMetrics(f);
   if(fm.stringWidth(TITLE)>w) continue;
   if(fm.getHeight()>partsize) continue;
   break;
  }
 bigfont=f;
 
 for(i=25;i>0;i--)
  {
   f=new Font("Helvetica",Font.PLAIN,i);
   fm = getFontMetrics(f);
   if(fm.getHeight()>partsize/2) continue;
   if(fm.stringWidth("minute ")>w/4) continue;
   break;
  }
 smallfont=f;
 startgr=fm.stringWidth("minute ");
 /* najdeme konec grafu podle maxima z hitu */
 int maxsize=0;
 maxsize=Math.max(maxsize,fm.stringWidth(new Integer(minute).toString()));
 maxsize=Math.max(maxsize,fm.stringWidth(new Integer(minuteavg).toString()));
 maxsize=Math.max(maxsize,fm.stringWidth(new Integer(hour).toString()));
 maxsize=Math.max(maxsize,fm.stringWidth(new Integer(houravg).toString()));
 maxsize=Math.max(maxsize,fm.stringWidth(new Integer(day).toString()));
 maxsize=Math.max(maxsize,fm.stringWidth(new Integer(dayavg).toString()));
 maxsize=Math.max(maxsize,fm.stringWidth(new Integer(week).toString()));
 maxsize=Math.max(maxsize,fm.stringWidth(new Integer(weekavg).toString()));
 maxsize=Math.max(maxsize,fm.stringWidth(new Integer(month).toString()));
 maxsize=Math.max(maxsize,fm.stringWidth(new Integer(monthavg).toString()));
 // maxsize=Math.max(maxsize,fm.stringWidth(new Integer(year).toString()));
 endgr=w-maxsize-fm.stringWidth(" ");
}

/* DRAW ! DRAW ! DRAW ! DRAW ! DRAW ! DRAW ! DRAW ! DRAW ! DRAW ! */

private void drawit()
{
  int partsize=h/PARTS;
  /* pozadi */
  draw.setColor(BACKGROUND);   
  draw.fillRect(0, 0, w, h);
  
  /* titulek */
  draw.setColor(TITLECOLOR);
  draw.setFont(bigfont);
  FontMetrics fm=getFontMetrics(bigfont);
  draw.drawString(TITLE,(w-fm.stringWidth(TITLE))/2,fm.getAscent());
  
  /* Copyright */
  fm=getFontMetrics(smallfont);
  draw.setFont(smallfont);
  draw.drawString(COPYRIGHT,(w-fm.stringWidth(COPYRIGHT))/2,partsize+fm.getAscent());
  
  /* prvni cast */
  drawpart(partsize*2,"minute",minute,minuteavg);
  drawpart(partsize*3,"hour",hour,houravg);
  drawpart(partsize*4,"day",day,dayavg);
  drawpart(partsize*5,"week",week,weekavg);
  drawpart(partsize*6,"month",month,monthavg);
  // drawpart(partsize*7,"year",year,0);
  repaint();
}

private void drawpart(int ypos,String title,int value,int valueavg)
{
  /* nejprve nakreslime kriz */
  int i;
  int partsize=h/PARTS;
  int step=(endgr-startgr)/10;
  draw.setColor(AXIS);
  
  for(i=0;i<=10;i++)
   {
    draw.drawLine(startgr+(i*step),ypos+AXISBORDER,startgr+(i*step),ypos+partsize-AXISBORDER);
   }
  draw.drawLine(startgr,ypos+partsize/2, startgr+10*step,ypos+partsize/2);
  
  /* a nakreslime nyni levy text */
  FontMetrics fm=getFontMetrics(smallfont);
  draw.setColor(LEGEND);
  draw.drawString(title,0,ypos + fm.getAscent()+(partsize-fm.getHeight()-fm.getLeading())/2);
  
  /* napiseme hodnoty */
  draw.setColor(NUMBERS);
  String s=new Integer(value).toString();
  draw.drawString(s,w-fm.stringWidth(s),ypos+fm.getAscent()+(partsize/2-fm.getHeight()-fm.getLeading())/2);
  s=new Integer(valueavg).toString();
  draw.drawString(s,w-fm.stringWidth(s),ypos+(partsize/2)+fm.getAscent()+(partsize/2-fm.getHeight()-fm.getLeading())/2);
  
  /* nakreslime graf */
  
  /* meritko */
  i=1;
  int maxval=Math.max(value,valueavg);
  while(maxval>=i)
     {i=i*10;}
  i/=10;
  draw.setColor(BAR);
  draw.fillRect(1+startgr,ypos+BARBORDER,
    (int) (value/(double)i*step),partsize/2-BARBORDER);

  draw.setColor(BARAVG);
  draw.fillRect(1+startgr,1+ypos+partsize/2,
    (int) (valueavg/(double)i*step),partsize/2-BARBORDER);
        
    
}

public void update(Graphics g)
{
 paint(g);
}

public void paint(Graphics g)
{
   g.drawImage(image, 0, 0, this);
}

private void loaddata()
{
 URL me=getCodeBase();
 String line;
 try{
 DataInputStream dis=new DataInputStream(new BufferedInputStream((
      new URL(me.getProtocol(),me.getHost(),port,"/"+server+"/livestats.txt")).openStream()));
 
 int what=0,value;          
 while ( (line = dis.readLine()) != null)
 {
  what++;
  value=Integer.parseInt(line);
  switch(what)
    {
    case 1:minute=value;break;
    case 2:minuteavg=value;break;
    case 3:hour=value;break;
    case 4:houravg=value;break;
    case 5:day=value;break;
    case 6:dayavg=value;break;
    case 7:week=value;break;
    case 8:weekavg=value;break;
    case 9:month=value;break;
    case 10:monthavg=value;break;
    case 11:year=value;break;    
    case 12:total=value;break;
    default:
    }    
 
 }
 dis.close();

    }
 catch (IOException e) {}      
}

public boolean mouseEnter(Event e, int x, int y){

	showStatus(APPLETINFO);

	return(true);

}//end mouseEnter

public String getAppletInfo()
{
   return APPLETINFO;
}    

    public String[][] getParameterInfo()

    {	

		String[][] info = {

            {"title",       "String",      "Main applet title"},

            {"copyright",      "String",      "2nd text line"},

            {"server",	"String", "Display statistics of which server?"},

            {"refresh",	"int (sec)",	"Check for new stats every XX sec."},
            
            {"background",  "\"RRGGBB\"", "color setup"},
            {"titlecolor",  "\"RRGGBB\"", "color setup"},
            {"axis",  "\"RRGGBB\"", "color setup"},
            {"legend",  "\"RRGGBB\"", "color setup"},
            {"numbers",  "\"RRGGBB\"", "color setup"},
            {"bar",  "\"RRGGBB\"", "color setup"},
            {"baravg",  "\"RRGGBB\"", "color setup"}



        };

        return info;

    }

    private Color parseColorString(String colorString)
    {


        if(colorString.length()==6){
            int R = Integer.valueOf(colorString.substring(0,2),16).intValue();
            int G = Integer.valueOf(colorString.substring(2,4),16).intValue();
            int B = Integer.valueOf(colorString.substring(4,6),16).intValue();
            return new Color(R,G,B);
        }
        else return Color.lightGray;
    }
  public boolean mouseDown(Event evt, int x, int y)

  {
        try{
            getAppletContext().showDocument(new URL("http://ncic.netmag.cz/apps/nase/livewebstats.html"));
           }
        catch (MalformedURLException e) {};

	return true;

  }

}