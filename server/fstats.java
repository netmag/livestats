import java.io.*;
import java.util.*;


/* Struktura datoveho souboru .st */
/*
 dos.writeLong(nmaint); - cas dalsi udrzby
 dos.writeLong(maint); - interval udrzby
 dos.writeInt(sumcache); - hity
 dos.writeInt(numbers.length); - pocet cisel
 for(int i=0;i<numbers.length;i++)
  dos.writeInt(numbers[i]); a cisla
*/

public class fstats implements Runnable /* floating statistics */

{

/* minimalni interval pro ukladani na disk */

public static final long SAVETIMER=60*1000*10; /* 10 minut */
public static final long MAXSTOPTIME=60*60*24*31*1000L; /* 1 mesic */

protected int numbers[]; /* pole pro jednotliva cisla */
private long nmaint,maint;
private int sumcache;
private String fname;

fstats(String fn,int size, long maint)
{
 numbers=new int[size];
 nmaint=System.currentTimeMillis()+maint;
 this.maint=maint;
 sumcache=0;
 fname=fn;
}

fstats(String fn) throws IOException
{
 DataInputStream dis=null;
 int i;
 fname=fn;
 dis=new DataInputStream(new BufferedInputStream(new FileInputStream(fname))); 
 nmaint=dis.readLong();
 maint=dis.readLong();
 sumcache=dis.readInt();
 int total=dis.readInt();
 numbers=new int[total];
 for(i=0;i<total;i++)
  numbers[i]=dis.readInt();
 dis.close(); 
 long now=System.currentTimeMillis();
 if(now>nmaint)
   {
   /* promeskali jsme udrzbu! */
   long howmany;
   if(now-nmaint<MAXSTOPTIME)
      {
      howmany=(now-nmaint)/maint;
      howmany++;
      }
     else { System.out.println("Statsdaemon did not work more than 1 month. No maint done.");
            howmany=1; 
            /* moc dlouho mimo provoz - asi si nekdo hraje s datumem */
          }
   for(long j=0;j<howmany;j++)
    maint();
   return; 
   }

   /* test, zda nejaky pitomec nenastavil datum moc dopredu */
   /* coz bude delat potize pri jeho nastaveni spravne a restartu */
   if (nmaint>now+maint) { maint();
                           System.out.println("Warning: time moved to past");
                         };
}

/* Save a maint() runner */
public void run() 
{
 long savetimer=System.currentTimeMillis()+SAVETIMER;
 long now;
 while(true)
 {
  now=System.currentTimeMillis();
  if(now>nmaint) { maint();}
  if(now>savetimer) {save();savetimer=now+SAVETIMER;}
  now=System.currentTimeMillis();
  try{
  Thread.sleep(Math.min(nmaint-now,savetimer-now));  
  }
  catch(InterruptedException e) {System.out.println("Thread servicing "+fname+" interrupted. Saving+Exiting.");
                                 save();
                                 return;
                                }
 }
}

public synchronized void save()
{
 DataOutputStream dos=null;
 try{
 dos=new DataOutputStream(new BufferedOutputStream(new FileOutputStream(fname)));
 dos.writeLong(nmaint);
 dos.writeLong(maint);
 dos.writeInt(sumcache);
 dos.writeInt(numbers.length);
 for(int i=0;i<numbers.length;i++)
  dos.writeInt(numbers[i]);
 dos.close(); 
 }
 catch (Exception e) {if(dos!=null) try {dos.close();}catch (Exception f) {} }
}

public synchronized void inc()
{
 numbers[numbers.length-1]++;
 sumcache++;
}

public synchronized void maint()
{
 int i;
 sumcache-=numbers[0];
 for(i=0;i<numbers.length-1;i++)
   numbers[i]=numbers[i+1];
 nmaint=System.currentTimeMillis()+maint;
 numbers[numbers.length-1]=0;
}

public synchronized void sethits(int hits)
{
 int every=hits/numbers.length;
 for(int i=0;i<numbers.length;i++)
   numbers[i]=every;
 sumcache=numbers.length*every;
}


/* kasleme na monitor, v nejhorsim pripade dostaneme predchozi
statistiku, coz nas nezabije */

public int gethits()
{
 return sumcache;
}

public int getcurrent()
{
 return numbers[numbers.length-1];
}

public int getavg()
{
 return sumcache/numbers.length;
}


}
