import java.io.*;
import java.util.*;

public class TimeKiller implements Runnable
    {

    private Thread targetThread;
    private long millis;
    private Thread watcherThread;
    private boolean loop;
    private boolean enabled;

    /// Constructor.  Give it a thread to watch, and a timeout in milliseconds.
    // After the timeout has elapsed, the thread gets killed.  If you want
    // to cancel the kill, just call done().
    public TimeKiller( Thread targetThread, long millis )
	{
	this.targetThread = targetThread;
	this.millis = millis;
	watcherThread = new Thread( this );
	enabled = true;
	watcherThread.start();
	// Hack - pause a bit to let the watcher thread get started.
	try
	    {
	    Thread.sleep( 1000 );
	    }
	catch ( InterruptedException e ) {}
	}
    
    /// Constructor, current thread.
    public TimeKiller( long millis )
	{
	this( Thread.currentThread(), millis );
	}

    /// Call this when the target thread has finished.
    public synchronized void done()
	{
	loop = false;
	enabled = false;
	notify();
	}
    
    /// Call this to restart the wait from zero.
    public synchronized void reset()
	{
	loop = true;
	notify();
	}
    
    /// Call this to restart the wait from zero with a different timeout value.
    public synchronized void reset( long millis )
	{
	this.millis = millis;
	reset();
	}

    /// The watcher thread - from the Runnable interface.
    // This has to be pretty anal to avoid monitor lockup, lost
    // threads, etc.
    public synchronized void run()
	{
	Thread me = Thread.currentThread();
	me.setPriority( Thread.MAX_PRIORITY );
	if ( enabled )
	    {
	    do
		{
		loop = false;
		try
		    {
		    wait( millis );
		    }
		catch ( InterruptedException e ) {}
		}
	    while ( enabled && loop );
	    }
	if ( enabled && targetThread.isAlive() )
	    targetThread.stop();
	}


    /*
    /// Test routine.
    public static void main( String[] args )
	{
	System.out.println( (new Date()) + "  Setting ten-second timeout..." );
	TimeKiller tk = new TimeKiller( 10000 );
	try
	    {
	    System.out.println(
		(new Date()) + "  Starting twenty-second pause..." );
	    Thread.sleep( 20000 );
	    System.out.println(
		(new Date()) + "  Another twenty-second pause..." );
	    Thread.sleep( 20000 );
	    }
	catch ( InterruptedException e )
	    {
	    System.out.println(
		(new Date()) + "  Caught InterruptedException" );
	    }
	catch ( ThreadDeath e )
	    {
	    System.out.println( (new Date()) + "  Caught ThreadDeath" );
	    throw e;
	    }
	System.out.println( (new Date()) + "  Oops - pauses finished!" );
	}
        */

    }
