import java.io.*;
import java.net.*;

public class httpreq extends Thread
{

   private Socket socket = null;
   private String data;
   private statsdaemon daemon;
   public static byte gif[];
   

public static void loadgif(String what)
{
 try{
 File f=new File(what);
 if(!f.canRead()) return;
 gif=new byte[(int)f.length()];
 DataInputStream dis=new DataInputStream(new FileInputStream(what));
 dis.readFully(gif);
 dis.close();
 System.out.println(what+" loaded. Size="+gif.length);
 }
 catch (IOException e) {gif=null;};
 
}      
   
    httpreq(Socket socket,String data) {
        super("httpd-client");
        this.data=data;
        this.socket=socket;
    }

    httpreq(Socket socket, statsdaemon d) {
        super("httpd-client");
        this.data=null;
        this.daemon=d;
        this.socket=socket;
    }
        
 public void run() {
    boolean http10=true;
    boolean wantgif=false;
    try { DataInputStream in=new DataInputStream(new BufferedInputStream(socket.getInputStream()));
         DataOutputStream ou=new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
    /* precteme radku GET / */
    
    String req=in.readLine();
    if (req.lastIndexOf(" HTTP/")==-1) http10=false;
    if (req.lastIndexOf("gif")>-1) wantgif=true;
    if (wantgif && gif==null) wantgif=false;
     
    /*
    String req2=req.substring(req.indexOf('/'),req.lastIndexOf(' '));
    System.out.println("req2="+req2); 
    
    */
                                   
       /* HTTP 1.0+ - musime precist zbytek volovin az do prazdne radky */
       if(http10) { while(in.readLine().length()!=0);
                    /* posleme hlavicku */
                    ou.writeBytes("HTTP/1.0 200 OK\r\n");
                    ou.writeBytes("Server: "+statsdaemon.VERSION+"\r\n");
                    if (wantgif) ou.writeBytes("Content-type: image/gif\r\n"); else
                                 ou.writeBytes("Content-type: text/plain\r\n");
                    ou.writeBytes("Pragma: no-cache\r\n");
                    ou.writeBytes("\r\n");
       }
      /* tuknout na statistiku */
      data=daemon.handlerequest(req); 
      /* posleme data */ 
      if(wantgif) ou.write(gif,0,gif.length);
        else ou.writeBytes(data);
      ou.close();
    }
    catch (Exception e)
    {}
    
     /* zkusime zavrit socket */
     
     try {
     socket.close();} catch (IOException e) {};
     socket=null;
     return;
    }   
}