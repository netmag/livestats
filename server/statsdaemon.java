import java.net.*;
import java.io.*;
import java.util.*;

public class statsdaemon
{

 public static final String VERSION="LiveStats daemon 1.12 (c) hsn@cybermail.net 1997-8.";
 private ServerSocket server;
 public volatile boolean listening=true;
 private fstats hour[],day[],week[],month[],year[],total[];
 private Thread threads[];
 private String names[];
 private String shutdown;
 public final static long CLIENTTIMEOUT=3*60*1000L;
 
 public statsdaemon (int port) throws IOException
 {
  System.out.println(VERSION);
  
  server=new ServerSocket(port);
  System.out.println("Starting server on port "+port);
  
  shutdown="terminate";
  /* inicializace z konfiguracniho fajlu */
  System.out.println("Parsing config file: stats.cnf");
  DataInputStream dis=new DataInputStream(new BufferedInputStream(new FileInputStream("stats.cnf")));
  String line,token;
  StringTokenizer st;
  while ( (line=dis.readLine())!=null )
   {
     st=new StringTokenizer(line);
     if (st.hasMoreTokens()) token=st.nextToken();
                            else
                              continue;
     if(token.equalsIgnoreCase("shutdownstring")) 
         {
          if (st.hasMoreTokens()) token=st.nextToken();
                                 else continue;
          shutdown=token;continue;
         }
     if(token.equalsIgnoreCase("statsfor")) 
           {
              if (st.hasMoreTokens()) token=st.nextToken();
                                    else continue;

               /* pripravit na posledni pozici volne misto */                                    
               if(names==null) names=new String[1];
                else
                 {
                  String tmp[]=names;
                  names=new String[names.length+1];
                  for(int i=0;i<tmp.length;i++)
                   names[i]=tmp[i];
                  tmp=null;
                 }
                 names[names.length-1]=token;
                 continue;
           }
    System.out.println("Unknown keyword: "+token);       
   }                              

  
  dis.close();
  if(names==null) throw new IOException("Nejsou definovany zadne servery!");
  
  
  System.out.println("Init server & restore old status");
  /* Zacina inicializace serveru */
  
  /* priprava poli */
  hour=new fstats[names.length];
  day=new fstats[names.length];
  week=new fstats[names.length];
  month=new fstats[names.length];
  year=new fstats[names.length];
  total=new fstats[names.length];    
  
  /* inicializace statistik */
  for(int i=0;i<names.length;i++)
  {
  File f=new File(names[i]);
  if(!f.exists()) f.mkdir();
  token=names[i]+File.separator+"hour.st";
  try{
  hour[i]=new fstats(token);
  }
  catch (IOException e) { hour[i]=new fstats(token,60,1000*60); }

  token=names[i]+File.separator+"day.st";
  try{
  day[i]=new fstats(token);
  }
  catch (IOException e) { day[i]=new fstats(token,24,1000*60*60); }

  token=names[i]+File.separator+"week.st";
  try{
  week[i]=new fstats(token);
  }
  catch (IOException e) { week[i]=new fstats(token,7,1000L*60*60*24); }
    
  token=names[i]+File.separator+"month.st";
  try{
  month[i]=new fstats(token);
  }
  catch (IOException e) { month[i]=new fstats(token,30,1000*60*60*24); }

  token=names[i]+File.separator+"year.st";  
  try{
  year[i]=new fstats(token);
  }
  catch (IOException e) { year[i]=new fstats(token,12,1000L*60*60*24*30); }
  
  token=names[i]+File.separator+"total.st";  
  try{
  total[i]=new fstats(token);
  }
  catch (IOException e) { total[i]=new fstats(token,1,1000L*60*60*24*365*100); 
                          total[i].sethits(year[i].gethits());
                        }

  /* SAVE ! */  
  hour[i].save();
  day[i].save();
  week[i].save();
  month[i].save();
  year[i].save();
  total[i].save();
  System.out.println(names[i]+" init done. Total hits="+total[i].gethits());
  } /* for */
    
  
 }

public static void main(String[] args) throws IOException {
 statsdaemon sd;
 int port;
 httpreq.loadgif("empty.gif");
 port=81;
 if (args.length>0) port=(Integer.valueOf(args[0])).intValue();

 sd=new statsdaemon(port);
 sd.loop();
 
}  

 public void loop()
 {
   /* pripravujeme thready */
   threads=new Thread[names.length*6];
   for(int i=0;i<names.length;i++)
   {
   threads[i*6]=new Thread(hour[i]);
   threads[i*6+1]=new Thread(day[i]);
   threads[i*6+2]=new Thread(month[i]);
   threads[i*6+3]=new Thread(year[i]);
   threads[i*6+4]=new Thread(week[i]);
   threads[i*6+5]=new Thread(total[i]);
   }
   
   /* startujeme thready */
   for(int i=0;i<threads.length;i++)
      threads[i].start();
   
   /* startujeme! */
   
   System.out.println("Running...");
   while (listening) {
            Thread t;
            Socket clientSocket = null;
            try {
                clientSocket = server.accept();
                } catch (IOException e) {
                System.err.println("Accept failed: ");
                continue;
            }
            t=new httpreq(clientSocket,this);
            new TimeKiller(t,CLIENTTIMEOUT);
            t.start();
   }  /* listen */
   
 }
 
 public String handlerequest(String req)
 {
  boolean update=true;
  if(req.indexOf("check")>0) update=false;
  int i=-1;
  
  for(int j=0;j<names.length;j++)
   if(req.indexOf(names[j])>0) {i=j;break;}
  if(i==-1) i=0; /* default */
  
  if(update)
    {
	  hour[i].inc();
	  day[i].inc();
	  month[i].inc();
	  week[i].inc();
	  year[i].inc();
	  total[i].inc();
    }
  if(req.indexOf(shutdown)>0) {  listening=false; shutdown();}

  return getStats(i);
 }
 
private void shutdown() 
{
 System.out.println("Starting server shutdown...");
 
 for(int i=0;i<threads.length;i++)
  threads[i].interrupt();
 System.out.println("Send interrupt to all active threads.\nWaiting 15 sec for finish.");  
  
  try{
   Thread.sleep(1000*30);  /* 15 sekund */
  }
  catch(InterruptedException e) {  }
  
 int alive=0;
 for(int i=0;i<threads.length;i++)
  if(threads[i].isAlive()) alive++;

 if(alive>0) {
               System.out.println(alive+" threads still alive after 15 sec delay... forced saving.");
               
                 /* SAVE ! */  
                 for(int i=0;i<hour.length;i++)
                 {
		  hour[i].save();
		  day[i].save();
		  week[i].save();
		  month[i].save();
		  year[i].save();
		  total[i].save();
                 }
             }  
 System.out.println("Shuting down JVM");            
 System.exit(0);
}
 private String getStats(int i)
 {
  String s="";
  
  /* minute */
  s+=hour[i].getcurrent(); /* neklouzavy, ale presny udaj */
  s+="\r\n";
  s+=hour[i].getavg();
  s+="\r\n";
  
  /* hour */
  s+=hour[i].gethits();
  s+="\r\n";
  s+=day[i].getavg();
  s+="\r\n";
  
  /* day */
  s+=day[i].gethits();
  s+="\r\n";
  s+=month[i].getavg();
  s+="\r\n";
  
  /* week */
  s+=week[i].gethits();
  s+="\r\n";
  s+=month[i].gethits()/4;
  s+="\r\n";
  
  /* month */
  s+=month[i].gethits();
  s+="\r\n";
  s+=year[i].getavg();
  s+="\r\n";
  
  /* year */
  s+=year[i].gethits();
  s+="\r\n";
  
  /* total */
  s+=total[i].gethits();
  s+="\r\n";
  
  return s;
 }
}